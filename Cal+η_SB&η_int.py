# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd

CenterLocationData = pd.read_excel('附件.xlsx')
CenterLocationMatrix = CenterLocationData.to_numpy()
'''#每面镜子中心的坐标作为字典的键，值在此处先留空，
未来会使每面镜子顶点及中心的地面投影坐标构成的矩阵（二维数组）作为字典的值: '''
Dict={tuple(elem): None for elem in CenterLocationMatrix}#使用字典推导来遍历二维数组的每一个子数组，将每一个子数组转换为元组并作为键
# print(CenterLocationMatrix)
NumMir = CenterLocationMatrix.shape[0]  # 定日镜数目
phi = 39.4  # 当地纬度
z=4
FlagS = 0
FlagB = 1
M = 0
H=80
TempS = 0
As = [0, 0, 0]
Ab = [0, 0, 0]
Ar = [0, 0]
yeardays = 365  # a设一年为365天
equinox = 31+28+21  # a春分日（3月21日）距离年初的天数（2月记28天）
lm=wm=6
N=1E9
# 以春分日为第0天起算的天数D：
# 初始化一个空的行向量
D_matrix = np.array([])
for month in range(1, 13):
    if month < 3:
        if month == 1:
            D = yeardays - equinox + 21  # 以春分日为第0天起算，一月21日
        else:
            D = yeardays - equinox + 21 + 31  # 二月21日
    else:
        if month == 3:
            D = 0
        elif month == 4:
            D = 30 * 1 + 1
        elif month == 5:
            D = 30 * 2 + 1
        elif month == 6:
            D = 30 * 3 + 2
        elif month == 7:
            D = 30 * 4 + 2
        elif month == 8:
            D = 30 * 5 + 3
        elif month == 9:
            D = 30 * 6 + 4
        elif month == 10:
            D = 30 * 7 + 4
        elif month == 11:
            D = 30 * 8 + 5
        elif month == 12:
            D = 30 * 9 + 5

    # 使用numpy的append函数，将D的值添加到行向量的最右侧
    D_matrix = np.append(D_matrix, D)

# print(D_matrix)


def Delta(D):  # 计算太阳赤纬角
    return np.arcsin(np.sin(2 * np.pi * D / 365) * np.sin(2 * np.pi / 360 * 23.45))


def Omega(ST):  # 计算太阳时角
    return np.pi/12*(ST-12)


def HS(delta, omega):  # 计算太阳高度角α_s
    # global phi
    # array=np.empty(2)
    # array[0]=delta
    # array[1]=np.arcsin(np.cos(delta) * np.cos(phi) * np.cos(omega) + np.sin(delta) * np.sin(phi))
    return np.arcsin(np.cos(delta) * np.cos(phi) * np.cos(omega) + np.sin(delta) * np.sin(phi))


def SinHS(delta, omega):  # sin(α_s)
    global phi
    return np.cos(delta) * np.cos(phi) * np.cos(omega) + np.sin(delta) * np.sin(phi)
# def PS(HSarray):
#     global phi
#     delta=HSarray[0]
#     hs=HSarray[1]
#     return np.arccos((np.sin(delta) - np.sin(hs) * np.sin(phi)) / (np.cos(hs) * np.cos(phi)))


def PS(delta, omega):  # 计算太阳方位角γ_s
    # hs=np.arcsin(np.cos(delta) * np.cos(phi) * np.cos(omega) + np.sin(delta) * np.sin(phi)
    hs = HS(delta, omega)
    return np.arccos((np.sin(delta) - np.sin(hs) * np.sin(phi)) / (np.cos(hs) * np.cos(phi)))
# print (PS(HS(Delta(0), Omega(9))))
# print(PSS(Delta(0), Omega(9)))
# for ST in range (9,16,1.5):#左闭右开区间 ，范围是当地时间 九点至15点


def L(hs, ps):  # 计算主入射光线的单位向量
    Coordinate = np.empty(3)
    Coordinate[0] = np.cos(hs)*np.sin(ps)
    Coordinate[1] = np.cos(hs)*np.sin(ps)
    Coordinate[2] = np.sin(hs)
    # Coordinate=np.array([])
    # Coordinate=np.append(Coordinate,np.cos(hs)*np.sin(ps))
    # Coordinate=np.append(Coordinate,np.cos(hs)*np.sin(ps))
    # Coordinate=np.append(Coordinate,np.sin(hs))
    return Coordinate
# Coordinate=L(HS(Delta(0), Omega(9)),PS(Delta(0), Omega(9)))
# print(Coordinate)


def ModuleLength(v):  # 计算一个向量的模长
    return np.linalg.norm(v)


def R(x, y):  # 计算主光线由镜中心的反射向量的单位向量
    global z,H
    Coordinate = np.empty(3)
    Coordinate[0] = -x
    Coordinate[1] = -y
    Coordinate[2] = H-z
    ml = ModuleLength(Coordinate)  # R的模长
    # 向量单位化：
    Coordinate[0] = Coordinate[0]/ml
    Coordinate[1] = Coordinate[1]/ml
    Coordinate[2] = Coordinate[2]/ml
    return Coordinate


def N(L, R):  # 计算镜面法向量的单位向量
    a, b, c = L
    rox, roy, roz = R
    Coordinate = np.array([a+rox, b+roy, c+roz])
    ml = ModuleLength(Coordinate)  # R的模长
    nx, ny, nz = Coordinate
    Coordinate = np.array([nx/ml, ny/ml, nz/ml])
    return Coordinate


def Apex(x, y, N):#计算顶点定日镜坐标系坐标【x,y,z为附件及第一问假设所给定的定日镜中心的镜场坐标系坐标】
    global z,lm,wm
    nx, ny, nz = N
    xp1 = x-lm/2*ny/(nx**2+ny**2)**(1/2)+wm/2*nz*nx/(nx**2+ny**2)**(1/2)
    yp1 = y+lm/2*ny/(nx**2+ny**2)**(1/2)+wm/2*nz*nx/(nx**2+ny**2)**(1/2)
    zp1 = z-wm/2*(1-nz**2)**(1/2)
    P1 = np.array([xp1, yp1, zp1])#
    xp2 = x-lm/2*ny/(nx**2+ny**2)**(1/2)-wm/2*nz*nx/(nx**2+ny**2)**(1/2)
    yp2 = y+lm/2*ny/(nx**2+ny**2)**(1/2)-wm/2*nz*nx/(nx**2+ny**2)**(1/2)
    zp2 = z+wm/2*(1-nz**2)**(1/2)
    P2 = np.array([xp2, yp2, zp2])#
    xp3 = x+lm/2*ny/(nx**2+ny**2)**(1/2)-wm/2*nz*nx/(nx**2+ny**2)**(1/2)
    yp3 = y-lm/2*ny/(nx**2+ny**2)**(1/2)-wm/2*nz*nx/(nx**2+ny**2)**(1/2)
    zp3 = z+wm/2*(1-nz**2)**(1/2)
    P3 = np.array([xp3, yp3, zp3])#
    xp4 = x+lm/2*ny/(nx**2+ny**2)**(1/2)+wm/2*nz*nx/(nx**2+ny**2)**(1/2)
    yp4 = y-lm/2*ny/(nx**2+ny**2)**(1/2)+wm/2*nz*nx/(nx**2+ny**2)**(1/2)
    zp4 = z-wm/2*(1-nz**2)**(1/2)
    P4 = np.array([xp4, yp4, zp4])#
    P=np.array([P1,P2,P3,P4])
    return P
# def Ground(L, x, y,z,Apex):  # 计算定日镜的中心和顶点沿着主入射光线在地面上的投影坐标【x,y,z为附件及第一问假设所给定的定日镜中心的镜场坐标系坐标】
def ifinShadow(Vex,Xis,Yis):
        # for Vex in DICT.value:
            #取所有元素中x/y最小值
        P1,P2,P3,P4,L=Vex
        xp1,yp1=P1
        xp2,yp2=P2
        xp3,yp3=P3
        xp4,yp4=P4   
        Bx=xp2
        By=yp2
        Ax=xp3
        Ay=yp3
        Dx=xp4
        Dy=yp4
        Ex=Xis
        Ey=Yis
        ax=Bx-Ax
        ay=By-Ay
        bx=Dx-Ax
        by=Dy-Ay
        cx=Ex-Ax
        cy=Ey-Ay
        aa=ax**2+ay**2
        ab=ax*bx+ay*by
        ac=ax*cx+ay*cy
        bb=bx**2+by**2
        bc=bx*cx+by*cy
        u=(aa*bc-ab*ac)/(aa*bb-ab**2)
        v=(bb*ac-bc*ab)/(aa*bb-ab**2)
        # uv=np.array([u,v])
        # return uv
# def ifinShadow(uv):        
        # u,v=uv
        
        if u>0: return [False,L]
        if v>1: return [False,L]
        return [True,L]

# def HowManyShadow(judgement):

def Ground(L,Apex):  # 计算定日镜顶点沿着主入射光线在地面上的投影坐标

    a,b,c=L#主光线向量
    P1,P2,P3,P4=Apex#顶点定日镜坐标系坐标
    xp1,yp1,zp1=P1
    xp2,yp2,zp2=P2
    xp3,yp3,zp3=P3
    xp4,yp4,zp4=P4
    
    #定日镜中心坐标
    # xos=-a/b*z+x
    # yos=-b/c*z+y
    # OS=np.array([xos,yos])
    #定日镜顶点P1坐标->PS1
    xps1=-a/b*zp1+xp1
    yps1=-b/c*zp1+yp1
    PS1=np.array([xps1,yps1])
    #定日镜顶点P2坐标->PS2
    xps2=-a/b*zp2+xp2
    yps2=-b/c*zp2+yp2
    PS2=np.array([xps2,yps2])

    #定日镜顶点P3坐标->PS3
    xps3=-a/b*zp3+xp3
    yps3=-b/c*zp3+yp3
    PS3=np.array([xps3,yps3])

    #定日镜顶点P4坐标->PS4
    xps4=-a/b*zp4+xp4
    yps4=-b/c*zp4+yp4
    PS4=np.array([xps4,yps4])
    
    # PSOS=np.array([PS1,PS2,PS3,PS4,OS]) 
    PS=np.array([PS1,PS2,PS3,PS4,L]) 

    return PS
'''算出某一时刻每面镜子对应的地面投影（顶点坐标Ground()生成的PS矩阵）范围，存储到~.CSV文件~ -> 【字典】 <- 中：'''
def ALLPS(Dict,D,ST):
    '''每面镜子中心的坐标作为字典的键；
    将对应镜子顶点的地面投影坐标构成的二维矩阵填充为字典Dict的值： '''
    COPY1=Dict.copy()
    COPY2=Dict.copy()
    for elem in COPY1:
        key=elem.key
        value=elem.value
        value=Ground(L(HS(Delta(D),Omega(ST)),PS(Delta(D),Omega(ST))),Apex(key[0], key[1], N(L(HS(Delta(D),Omega(ST)))),R(key[0],key[1])))
# return COPY1
    xemptysymbol=1
    yemptysymbol=1
    for Vex in COPY1.value:
        #取所有元素中x/y最小值
        P1,P2,P3,P4,L=Vex
        xp1,yp1=P1
        xp2,yp2=P2
        xp3,yp3=P3
        xp4,yp4=P4
        if xemptysymbol==1: 
            minx=xp1
            maxx=xp1
        minx=min(xp1,xp2,xp3,xp4,minx)
        maxx=max(xp1,xp2,xp3,xp4,maxx)
        xemptysymbol=xemptysymbol-1
        if yemptysymbol==1: 
            miny=yp1
            maxy=yp1
        miny=min(yp1,yp2,yp3,yp4,miny)
        maxy=max(yp1,yp2,yp3,yp4,maxy)
        yemptysymbol=yemptysymbol-1
        #取所有元素中x/y最大值
    global N
    '''生成随机光点'''
    # SPOTs=np.array([])#投撒的随机光点的集合
    JUDGE=np.array([])
    Ngrd=Ns=Nb=uvCount=0
    for i in range(N):
        '''一个光点的坐标：'''
        Xis=minx+(maxx-minx)*np.random.rand()
        Yis=miny+(maxy-miny)*np.random.rand()
        # SPOTelem=np.array([])
        # SPOTelem=np.append(SPOTelem,Xis)
        # SPOTelem=np.append(SPOTelem,Yis)
        # SPOTs=np.append(SPOTs,SPOTelem)
        for Vex in COPY1.value:
            #取所有元素中x/y最小值
            P1,P2,P3,P4=Vex
            xp1,yp1=P1
            xp2,yp2=P2
            xp3,yp3=P3
            xp4,yp4=P4   
            BOOL,L=ifinShadow(Vex, Xis, Yis)
            if(BOOL): 
                Ngrd=Ngrd+1
                continue
            else:
                JUDGEelem=np.array([])
                JUDGEelem=np.append(JUDGEelem,Vex)
                JUDGEelem=np.append(JUDGEelem,L)
                JUDGE=np.append(JUDGE,Vex)
                uvCount=uvCount+1
        if uvCount>1:
            '''确定生成的某随机光点在镜上的实际位置的思路是，其坐标由从沿主入射光向量投影到地面上的逆过程（即Ground()的逆过程）和确定的镜面法向量N()两个因素共同决定与计算得出'''
            
            
                
        

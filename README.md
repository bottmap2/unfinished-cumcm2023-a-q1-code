# Unfinished CUMCM2023-A Q1 Code



## References

> The algorithm is fully presented in this paper:

[1] https://kns.cnki.net/kcms2/article/abstract?v=J0BDVs0XdL1zgQBksbJVHd9j7aKgjduu5dvKDSrw4emPRGruUF_upErB8L0YOCnrHW2ZAxU4Ag3RXx-_LCvfLIseSScNKqd-QvkDmF3bSH8nvW8Y0hyLs7X7NCvW1GwQgMolMiuq1c4=&uniplatform=NZKPT&language=CHS

> Another very useful paper that helps to find the paper above:

[2] https://kns.cnki.net/kcms2/article/abstract?v=3uoqIhG8C44YLTlOAiTRKibYlV5Vjs7iy_Rpms2pqwbFRRUtoUImHQfdHpPJpJSSFZtr8Jo0KR6hVZY-nSrd-NGAjpDSvw9K&uniplatform=NZKPT
